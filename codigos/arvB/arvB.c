#include "arvB.h"

arvb_t *arvb_inicia(void){
  return NULL;
}

arvb_t *arvb_cria_pagina(int s, unsigned int d){
  if (!d) return NULL;
  arvb_t *a = (arvb_t *)malloc(sizeof(arvb_t));
  a->m=1;
  a->chaves = (int *)malloc(sizeof(int)*2*d);
  a->filhos = (arvb_t **)malloc(sizeof(arvb_t *)*(2*d+1));
  a->pai=NULL;
  a->folha=1; // true
  a->chaves[0] = s;
  for (int i=0; i<(2*d+1); i++) a->filhos[i]=NULL;
  return a;
}

void arvb_libera(arvb_t *a){
  if (!a) return;
  for (int i=0; i<=a->m; i++) arvb_libera(a->filhos[i]);
  free(a->chaves);
  free(a->filhos);
  free(a);
}

void arvb_imprime(arvb_t *a, int tab){
  if (!a) return;
  for(int i=0; i<a->m; i++){
    arvb_imprime(a->filhos[i],tab+1);
    for(int j=0; j<tab; j++) printf("----");
    printf("%d\n", a->chaves[i]);
  }
  arvb_imprime(a->filhos[a->m],tab+1);
}

arvb_t *arvb_busca(arvb_t *a, int s){
  if (!a) return NULL;
  int i;
  for (i=0; i<a->m && a->chaves[i] < s; i++) ;
  if ( (i < a->m && a->chaves[i] == s) || a->folha ) return a; // mesmo se nao achar, retorna folha onde deveria estar
  return arvb_busca(a->filhos[i],s);
}

void insere_chave_em_pagina(arvb_t *a, int s, arvb_t *dir, unsigned int d){
  if (!d || (!a && !dir)) return;
  if (!a && dir){ // checa se tem que criar nova pagina (nova raiz)
    a = arvb_cria_pagina(s,d);
    a->filhos[1]=dir;
    a->folha=0;
    dir->pai=a;
    return;
  }
  if (a->m < (2*d)){ // checa se nao precisa particionar
    int j;
    for (j=0; j<a->m && a->chaves[j] < s; j++) ;
    for (int i=a->m; i>j; i--){
      a->chaves[i] = a->chaves[i-1];
      a->filhos[i+1] = a->filhos[i];
    }
    a->chaves[j] = s;
    a->filhos[j+1] = dir;
    a->m++;
    if (dir) dir->pai=a;
    return;
  }
  // se passou daqui, tem que particionar
  printf("INSERCAO CAUSOU PARTICIONAMENTO!\n");
 
  // cria pagina irma de a
  arvb_t *b = (arvb_t *)malloc(sizeof(arvb_t));
  b->m=d;
  b->chaves = (int *)malloc(sizeof(int)*2*d);
  b->filhos = (arvb_t **)malloc(sizeof(arvb_t *)*(2*d+1));
  b->pai=NULL;
  b->folha=a->folha;
  for (int i=0; i<(2*d+1); i++) b->filhos[i]=NULL;
  
  unsigned char flag=0; // false (sinaliza se a chave nova s entrou em b)
  // copia metade das chaves e filhos de a para b
  for (int i=d-1; i>=0; i--){
    if (s > a->chaves[d+i+flag] && !flag){
      flag=1;  // true
      b->chaves[i]=s;
      b->filhos[i+1]=dir;
      if (dir) dir->pai=b;
    } else {
      b->chaves[i]=a->chaves[d+i+flag];
      b->filhos[i+1]=a->filhos[1+d+i+flag];
      if (b->filhos[i+1]) b->filhos[i+1]->pai=b;
    }
  }
  
  // se nova chave nao entrou em b, insere em a (agora tem espaco)
  a->m=d;
  if (!flag) insere_chave_em_pagina(a,s,dir,d);
  
  // insere ultima chave de a no pai
  b->filhos[0]=a->filhos[d+1];
  if (b->filhos[0]) b->filhos[0]->pai=b;
  insere_chave_em_pagina(a->pai,a->chaves[d],b,d);
  a->m=d;
  
  // se criou uma nova raiz, fazer encadeamentos
  if (!(a->pai)){
    a->pai=b->pai;
    a->pai->filhos[0]=a;
  }
}

arvb_t *arvb_insere(arvb_t *a, int s, unsigned int d){
  if (!d) return a;
  if (!a) return arvb_cria_pagina(s,d); // se arvore vazia, cria primeiro no e insere s lá
  arvb_t *no = arvb_busca(a,s); // retorna no onde encontrou s ou onde s deveria entrar
  if (!no) return a; // seguranca
  if (!(no->folha)) return a; // achou um no interno -- note que nesse caso, s ja esta na arvore
  for (int i=0; i<no->m; i++) if (no->chaves[i] == s) return a; // achou s em uma folha -- s ja esta na arvore
  insere_chave_em_pagina(no,s,NULL,d);
  if (a->pai) a=a->pai; // posso ter subido a raiz na operacao da linha anterior
  return a;
}

void redistribuicao(arvb_t *a, int s, unsigned int d);
arvb_t *concatenacao(arvb_t *a, int s, unsigned int d);

arvb_t *decide_entre_concatenacao_ou_redistribuicao(arvb_t *a, unsigned int d){
  arvb_t *p=a->pai;
  int k;
  for (k=0; k<=p->m && p->filhos[k]!=a; k++) ;
  if (k>p->m) return a; // note que deve acontecer, algo deu errado se caiu aqui. retorno a por seguranca

  if      (k && p->filhos[k-1]->m > d) redistribuicao(p,p->chaves[k-1],d);
  else if (k<p->m && p->filhos[k+1]->m > d) redistribuicao(p,p->chaves[k],d);  
  else if (k) a = concatenacao(p,p->chaves[k-1],d);
  else        a = concatenacao(p,p->chaves[0],d);
  return a;
}

void redistribuicao(arvb_t *a, int s, unsigned int d){
  //     [a] :  s
  //           / \
  //         [p]  [q]
  if (!a || !d) return;
  arvb_t *p, *q;
  int j;
  for (j=0; j<a->m && a->chaves[j] != s; j++) ;
  if (j>=a->m) return; // nao achou s em a
  printf("REMOCAO CAUSOU REDISTRIBUICAO!\n");
  p=a->filhos[j];
  q=a->filhos[j+1];
  if (p->m < d){
    p->chaves[p->m++]=s;
    p->filhos[p->m] = q->filhos[0];
    if (p->filhos[p->m]) p->filhos[p->m]->pai=p;
    a->chaves[j] = q->chaves[0];
    for (int i=0; i<q->m-1; i++){
      q->chaves[i] = q->chaves[i+1];
      q->filhos[i] = q->filhos[i+1]; 
    }
    q->filhos[q->m-1]=q->filhos[q->m];
    q->m--;
  } else { // (q->m < d)
    for (int i=0; i<q->m; i++){
      q->chaves[i+1] = q->chaves[i];
      q->filhos[i+1] = q->filhos[i]; 
    }
    q->filhos[q->m+1]=q->filhos[q->m];
    q->m++;
    q->chaves[0]=s;
    q->filhos[0]=p->filhos[p->m];
    if (q->filhos[0]) q->filhos[0]->pai=q;
    a->chaves[j] = p->chaves[--p->m];
  }
}

arvb_t *concatenacao(arvb_t *a, int s, unsigned int d){
  //     [a] :  s
  //           / \
  //         [p]  [q]
  if (!a || !d) return a;
  arvb_t *p, *q;
  int j;
  for (j=0; j<a->m && a->chaves[j] != s; j++) ;
  if (j>=a->m) return a; // nao achou s em a
  p=a->filhos[j];
  q=a->filhos[j+1];
  if ((p->m + q->m + 1) > 2*d) return a; // nesse caso, nao pode concatenar
  printf("REMOCAO CAUSOU CONCATENACAO!\n");
  p->chaves[p->m++]=s;
  p->filhos[p->m]=q->filhos[0];
  if (p->filhos[p->m]) p->filhos[p->m]->pai=p;
  for (int i=0; i<q->m; i++){
    p->chaves[p->m++] = q->chaves[i];
    p->filhos[p->m] = q->filhos[i+1];
    if (p->filhos[p->m]) p->filhos[p->m]->pai=p;
  }
  free(q->chaves);
  free(q->filhos);
  free(q);
  if (!(a->pai) && a->m <= 1){ // se raiz com apenas uma chave, libera
    free(a->chaves);
    free(a->filhos);
    free(a);
    p->pai=NULL;
    return p;
  }
  // faz shift da direita para a esquerda, a partir de s (id j)
  for (int i=j; i<a->m-1; i++){
    a->chaves[i]=a->chaves[i+1];
    a->filhos[i+1]=a->filhos[i+2];
  }
  a->m--;
  if (a->m >= d || !(a->pai)) return a; // se nao ocorre underflow, nao ha mais o que fazer
  // se chegou aqui, tem que tratar o underflow
  return decide_entre_concatenacao_ou_redistribuicao(a,d);
}

arvb_t *arvb_remove(arvb_t *a, int s, unsigned int d){
  if (!a || !d) return a;
  arvb_t *p;
  arvb_t *no = arvb_busca(a,s); // retorna no onde encontrou s ou onde s deveria entrar
  if (!no) return a; // seguranca
  int j;
  for (j=0; j<no->m && no->chaves[j] != s; j++) ;
  if (j>=no->m) return a; // s nao estava em no
  if (!(no->folha)){ // se no interno, troca s com chave imediatamente maior e remove s da subarv
    p=a->filhos[j+1];
    while(!(p->folha)) p = p->filhos[0];
    a->chaves[j]=p->chaves[0];
    p->chaves[0]=s;
    return arvb_remove(a->filhos[j+1],s,d);
  }
  if (!(no->pai) && no->m <= 1){ // se raiz com apenas uma chave, libera
    free(no->chaves);
    free(no->filhos);
    free(no);
    return arvb_inicia();
  }
  // faz shift da direita para a esquerda, a partir de s (id j)
  for (int i=j; i<no->m-1; i++){
    no->chaves[i]=no->chaves[i+1];
    no->filhos[i+1]=no->filhos[i+2];
  }
  no->m--;
  if (no->m >= d || !(no->pai)) return a; // se nao ocorre underflow, nao ha mais o que fazer
  // se chegou aqui, tem que tratar o underflow
  a = decide_entre_concatenacao_ou_redistribuicao(no,d);
  while (a->pai) a=a->pai; // por convencao, retorna a raiz (posso ter mandado remover de subarv recursivamente)
  return a;
}
