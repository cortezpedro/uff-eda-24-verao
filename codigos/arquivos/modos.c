#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct _aluno{
  int mat;
  char nome[128];
  unsigned int nota;
} aluno_t;


void le_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fread(a,sizeof(aluno_t),1,f);
}

void salva_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fwrite(a,sizeof(aluno_t),1,f);
}

void imprime_aluno(aluno_t *a){
  if (!a) return;
  printf("[%d] %s: %d\n",a->mat,a->nome,a->nota);
}

size_t tamanho_arquivo(FILE *f){
  fseek(f,0,SEEK_END);
  size_t tam = ftell(f) / sizeof(aluno_t);
  rewind(f);
  return tam;
}

void imprime_conteudo_arquivo(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo(f);
  if (!n) return;
  aluno_t *dados = (aluno_t *)malloc(sizeof(aluno_t)*n);
  for (int i=0; i<n; i++) le_aluno(f,&dados[i]);
  for (int i=0; i<n; i++) imprime_aluno(&dados[i]);
  free(dados);
}

int main(void){

  aluno_t turma[]={
    {0,"Jonas",8},
    {1,"Marta",5},
    {2,"Francisco",3},
    {3,"Renato",9},
    {4,"Luan",6},
    {5,"Marcia",4},
    {6,"Maria",9}
  };
  const int dim = sizeof(turma) / sizeof(aluno_t);
  
  //////////////////////////////////////////////////////
  FILE *f = fopen("turma.bin","wb");
  if (!f) return 1;
  for (int i=0; i<dim; i++) salva_aluno(f,&turma[i]);
  fclose(f);
  printf("\n#################\n\n");
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////
  f = fopen("turma.bin","ab");
  if (!f) return 1;
  aluno_t a = {7,"Alex",10};
  salva_aluno(f,&a);
  fclose(f);
  printf("\n#################\n\n");
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  printf("\n#################\n\n");
  //////////////////////////////////////////////////////
  f = fopen("turma.bin","rb+");
  if (!f) return 1;
  a.mat = 8;
  strcpy(a.nome,"Joana");
  a.nota=7;
  fseek(f,5*sizeof(aluno_t),SEEK_SET);
  salva_aluno(f,&a);
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////
  f = fopen("turma.bin","wb+");
  if (!f) return 1;
  a.mat = 9;
  strcpy(a.nome,"Douglas");
  a.nota=5;
  fseek(f,3*sizeof(aluno_t),SEEK_SET);
  salva_aluno(f,&a);
  fclose(f);
  printf("\n#################\n\n");
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////
  f = fopen("turma.bin","ab+");
  if (!f) return 1;
  a.mat = 10;
  strcpy(a.nome,"Fernanda");
  a.nota=9;
  fseek(f,1*sizeof(aluno_t),SEEK_SET);
  salva_aluno(f,&a);
  fseek(f,3*sizeof(aluno_t),SEEK_SET);
  le_aluno(f,&a);
  fclose(f);
  printf("\n#################\n\n");
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  
  printf("\n"); imprime_aluno(&a);
  //////////////////////////////////////////////////////
  
  return 0;
}
