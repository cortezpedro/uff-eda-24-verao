#include <stdio.h>
#include <stdlib.h>

void merge_arquivo_binario(char *nomef1, char *nomef2, char *nomefout){
  FILE *f1, *f2, *fout;
  f1 = fopen(nomef1,"rb");
  if (!f1) { printf("Falhou ao tentar abrir %s.\n",nomef1); return; }
  f2 = fopen(nomef2,"rb");
  if (!f2) { fclose(f1); printf("Falhou ao tentar abrir %s.\n",nomef2); return; }
  fout = fopen(nomefout,"wb");
  if (!fout) { fclose(f1); fclose(f2); printf("Falhou ao tentar abrir %s.\n",nomefout); return; }
  
  /*
  EXERCICIO: Completar
  */
  
  fclose(f1);
  fclose(f2);
  fclose(fout);
}

void merge_arquivo_texto(char *nomef1, char *nomef2, char *nomefout){
  FILE *f1, *f2, *fout;
  f1 = fopen(nomef1,"r");
  if (!f1) { printf("Falhou ao tentar abrir %s.\n",nomef1); return; }
  f2 = fopen(nomef2,"r");
  if (!f2) { fclose(f1); printf("Falhou ao tentar abrir %s.\n",nomef2); return; }
  fout = fopen(nomefout,"w");
  if (!fout) { fclose(f1); fclose(f2); printf("Falhou ao tentar abrir %s.\n",nomefout); return; }
  
  int d1, d2;
  fscanf(f1,"%d",&d1);
  fscanf(f2,"%d",&d2);
  while (!feof(f1) && !feof(f2)){
    fprintf(fout,"%d\n", d1 < d2 ? d1 : d2);
    if (d1 < d2) fscanf(f1,"%d",&d1);
    else fscanf(f2,"%d",&d2);
  }
  
  FILE *pf = !feof(f1) ? f1 : f2;
  int d;
  if (pf==f1) d=d1;
  else d=d2;
  while (!feof(pf)){
    fprintf(fout,"%d\n", d);
    fscanf(pf,"%d",&d);
  }
  
  fclose(f1);
  fclose(f2);
  fclose(fout);
}

int main(int argc, char *argv[]){
  if (argc < 3) return 1;
  char *file1 = argv[1];
  char *file2 = argv[2];
  
  merge_arquivo_texto(file1,file2,"merge_result.txt");

  return 0;
}
