#include "avl.h"

avl_t *avl_RD(avl_t *a){
  printf("RD\n");
  avl_t *b = a->esq;
  avl_t *p = b->dir;
  b->dir = a;
  a->esq = p;
  
  unsigned int ae, ad;
  ae = a->esq ? a->esq->alt : 0;
  ad = a->dir ? a->dir->alt : 0;
  a->alt = (ae > ad ? ae : ad) + 1;
  a->fb = ad-ae;
  
  ae = b->esq ? b->esq->alt : 0;
  ad = a->alt;
  b->alt = (ae > ad ? ae : ad) + 1;
  b->fb = ad-ae;
  
  return b;
}

avl_t *avl_RE(avl_t *a){
  printf("RE\n");
  
  avl_t *b = a->dir;
  a->dir = b->esq;
  b->esq = a;
  
  unsigned int ae, ad;
  ae = a->esq ? a->esq->alt : 0;
  ad = a->dir ? a->dir->alt : 0;
  a->alt = (ae > ad ? ae : ad) + 1;
  a->fb = ad-ae;
  
  ae = a->alt;
  ad = b->dir ? b->dir->alt : 0;
  b->alt = (ae > ad ? ae : ad) + 1;
  b->fb = ad-ae;
  return b;
  
}

avl_t *avl_RDE(avl_t *a){
  a->dir = avl_RD(a->dir);
  return avl_RE(a);
}
avl_t *avl_RED(avl_t *a){
  a->esq = avl_RE(a->esq);
  return avl_RD(a);
}

avl_t *rotacoes(avl_t *a){
  // checa alturas
  unsigned int ae, ad;
  ae = a->esq ? a->esq->alt : 0;
  ad = a->dir ? a->dir->alt : 0;
  int fb = ad-ae;
  if (fb > -2 && fb < 2){ // se entrou aqui, nao precisa de rotacao
    a->fb=fb;
    a->alt=(ae > ad ? ae : ad) + 1;
    return a;
  }
  
  // chama rotacoes necessarias
  if (fb < -1){ // direita
    if (a->esq->fb <= 0){ // simples
      a = avl_RD(a);
    } else { // dupla
      a = avl_RED(a);
    }
  } else { // esquerda
    if (a->dir->fb >= 0){ // simples
      a = avl_RE(a);
    } else { // dupla
      a = avl_RDE(a);
    }
  }
  
  // atualiza alturas
  ae = a->esq ? a->esq->alt : 0;
  ad = a->dir ? a->dir->alt : 0;
  a->fb=ad-ae;
  a->alt=(ae > ad ? ae : ad) + 1;
  
  return a;
}

avl_t *avl_insere(avl_t *a, int d){
  if(!a){
    avl_t *no=(avl_t *)malloc(sizeof(avl_t));
    no->dado=d;
    no->esq=NULL; no->dir=NULL;
    no->fb=0;
    no->alt=1;
    return no;
  }
  if (d == a->dado) return a;
  
  if(d < a->dado)
    a->esq=avl_insere(a->esq,d);
  else
    a->dir=avl_insere(a->dir,d);
  
  return rotacoes(a);
}

avl_t *avl_remove(avl_t *a, int d){
  if(!a)return NULL;
  if(a->dado!=d){
    if(d < a->dado) a->esq=avl_remove(a->esq,d);
    else a->dir=avl_remove(a->dir,d);
    return rotacoes(a);
  }
  if(!(a->esq) && !(a->dir)){ free(a); return NULL;}
  if(!(a->esq) || !(a->dir)){
    avl_t *p=a;
    a = a->esq ? a->esq : a->dir;
    free(p);
    return a;
  }
  avl_t *p=a->esq;
  while(p->dir) p=p->dir;
  a->dado=p->dado;
  p->dado=d;
  a->esq=avl_remove(a->esq,d);
  return rotacoes(a);
}

void avl_libera(avl_t *a){
  if (!a) return;
  avl_libera(a->esq);
  avl_libera(a->dir);
  free(a);
}

avl_t *avl_busca(avl_t *a, int d){
  if(!a) return NULL;
  if (d == a->dado) return a;
  if(d < a->dado)
    return avl_busca(a->esq,d);
  return avl_busca(a->dir,d);
}

unsigned int avl_calcula_altura(avl_t *a){
  if (!a) return 0;
  unsigned int ae, ad;
  ae = avl_calcula_altura(a->esq);
  ad = avl_calcula_altura(a->dir);
  a->alt=(ae > ad ? ae : ad) + 1;
  return a->alt;
}

unsigned int avl_calcula_fb(avl_t *a){
  if (!a) return 0;
  unsigned int ae, ad;
  ae = a->esq ? a->esq->alt : 0;
  ad = a->dir ? a->dir->alt : 0;
  a->fb=ad-ae;
  return a->fb;
}

void avl_imprime_tab(avl_t *a, const unsigned int tab){
  for (unsigned int i=0; i<tab; i++) printf("----");
  if(!a) { printf("x\n"); return; }
  printf("%d\n",a->dado);
  avl_imprime_tab(a->esq,tab+1);
  avl_imprime_tab(a->dir,tab+1);
}

void avl_imprime_preordem(avl_t *a){
  if (!a) return;
  printf("%d ",a->dado);
  avl_imprime_preordem(a->esq);
  avl_imprime_preordem(a->dir);
}

void avl_imprime_altura_tab(avl_t *a, const unsigned int tab){
  for (unsigned int i=0; i<tab; i++) printf("----");
  if(!a) { printf("x\n"); return; }
  printf("%d\n",a->alt);
  avl_imprime_altura_tab(a->esq,tab+1);
  avl_imprime_altura_tab(a->dir,tab+1);
}

void avl_imprime_altura_preordem(avl_t *a){
  if (!a) return;
  printf("%d ",a->alt);
  avl_imprime_altura_preordem(a->esq);
  avl_imprime_altura_preordem(a->dir);
}

void avl_imprime_fb_tab(avl_t *a, const unsigned int tab){
  for (unsigned int i=0; i<tab; i++) printf("....");
  if(!a) { printf("x\n"); return; }
  printf("%d\n",a->fb);
  avl_imprime_fb_tab(a->esq,tab+1);
  avl_imprime_fb_tab(a->dir,tab+1);
}

void avl_imprime_fb_preordem(avl_t *a){
  if (!a) return;
  printf("%d ",a->fb);
  avl_imprime_fb_preordem(a->esq);
  avl_imprime_fb_preordem(a->dir);
}
