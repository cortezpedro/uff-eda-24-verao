#include "ab.h"

ab_t *ab_cria_no(int d){
  ab_t *no = (ab_t *)malloc(sizeof(ab_t));
  no->dado=d;
  no->esq=NULL;
  no->dir=NULL;
  return no;
}

void ab_libera(ab_t *a){
  if (!a) return;
  ab_libera(a->esq);
  ab_libera(a->dir);
  free(a);
}

unsigned int ab_calcula_altura(ab_t *a){
  if (!a) return 0;
  unsigned int ae, ad;
  ae = ab_calcula_altura(a->esq);
  ad = ab_calcula_altura(a->dir);
  return (ae > ad ? ae : ad) + 1;
}

void ab_imprime_preordem(ab_t *a){
  if (!a) return;
  printf("%d ",a->dado);
  ab_imprime_preordem(a->esq);
  ab_imprime_preordem(a->dir);
}
void ab_imprime_simetrica(ab_t *a){
  if (!a) return;
  ab_imprime_simetrica(a->esq);
  printf("%d ",a->dado);
  ab_imprime_simetrica(a->dir);
}
void ab_imprime_posordem(ab_t *a){
  if (!a) return;
  ab_imprime_posordem(a->esq);
  ab_imprime_posordem(a->dir);
  printf("%d ",a->dado);
}
void ab_imprime_largura(ab_t *a){
  typedef struct _fila {
    ab_t *no_arv;
    struct _fila *prox;
  } fila_t;
  fila_t *fila = (fila_t *)malloc(sizeof(fila_t));
  fila->no_arv = a;
  fila->prox = NULL;
  fila_t *fim = fila, *pf;
  ab_t *pa;
  while(fila){
    pa=fila->no_arv;
    printf("%d ",pa->dado);
    if (pa->esq){
      fim->prox = (fila_t *)malloc(sizeof(fila_t));
      fim = fim->prox;
      fim->no_arv = pa->esq;
      fim->prox = NULL;
    }
    if (pa->dir){
      fim->prox = (fila_t *)malloc(sizeof(fila_t));
      fim = fim->prox;
      fim->no_arv = pa->dir;
      fim->prox = NULL;
    }
    pf=fila;
    fila=fila->prox;
    free(pf);
  }
  return;
}

void ab_imprime_preordem_pilha(ab_t *a){
  typedef struct _pilha {
    ab_t *no_arv;
    struct _pilha *prox;
  } pilha_t;
  
  pilha_t *push(pilha_t *p, ab_t *a){
    pilha_t *no = (pilha_t *)malloc(sizeof(pilha_t));
    no->no_arv = a;
    no->prox = p;
    return no;
  }
  
  pilha_t *pop(pilha_t *p){
    pilha_t *pp=p;
    p=p->prox;
    free(pp);
    return p;
  }
  
  pilha_t *pilha = push(NULL,a);
  ab_t *pa;
  while(pilha){
    pa=pilha->no_arv;
    pilha = pop(pilha);
    printf("%d ",pa->dado);
    if (pa->dir){
      pilha=push(pilha,pa->dir);
    }
    if (pa->esq){
      pilha=push(pilha,pa->esq);
    }
  }
}
void ab_imprime_simetrica_pilha(ab_t *a){

  typedef struct _pilha {
    ab_t *no_arv;
    struct _pilha *prox;
  } pilha_t;
  
  pilha_t *push(pilha_t *p, ab_t *a){
    pilha_t *no = (pilha_t *)malloc(sizeof(pilha_t));
    no->no_arv = a;
    no->prox = p;
    return no;
  }
  
  pilha_t *pop(pilha_t *p){
    pilha_t *pp=p;
    p=p->prox;
    free(pp);
    return p;
  }
  
  pilha_t *pilha = push(NULL,a);
  ab_t *pa=a;
  while(pa->esq){
    pa=pa->esq;
    pilha = push(pilha,pa);
  }
  while(pilha){
    pa=pilha->no_arv;
    printf("%d ",pa->dado);
    pilha = pop(pilha);
    if (pa->dir){
      pa=pa->dir;
      pilha = push(pilha,pa);
      while(pa->esq){
        pa=pa->esq;
        pilha = push(pilha,pa);
      }
    }
  }
}
void ab_imprime_posordem_pilha(ab_t *a){

  typedef struct _pilha {
    ab_t *no_arv;
    struct _pilha *prox;
    int ja_foi_visto;
  } pilha_t;
  
  pilha_t *push(pilha_t *p, ab_t *a){
    pilha_t *no = (pilha_t *)malloc(sizeof(pilha_t));
    no->no_arv = a;
    no->prox = p;
    no->ja_foi_visto = 0;
    return no;
  }
  
  pilha_t *pop(pilha_t *p){
    pilha_t *pp=p;
    p=p->prox;
    free(pp);
    return p;
  }
  
  pilha_t *pilha = push(NULL,a);
  ab_t *pa=a;
  while(pa->esq){
    pa=pa->esq;
    pilha = push(pilha,pa);
  }
  while(pilha){
    pa=pilha->no_arv;
    if (pilha->ja_foi_visto || !(pa->dir)){
      printf("%d ",pa->dado);
      pilha = pop(pilha);
    } else {
      pilha->ja_foi_visto=1;
      pa=pa->dir;
      pilha = push(pilha,pa);
      while(pa->esq){
        pa=pa->esq;
        pilha = push(pilha,pa);
      }
    } 
  }
}

unsigned int ab_cheia(ab_t *a){
  if (!a) return 0;
  unsigned ae, ad;
  ae = ab_cheia(a->esq);
  ad = ab_cheia(a->dir);
  return ae == ad ? (ae+1) : 0;
}

ab_t * ab_espelho(ab_t *a){
  if (!a) return NULL;
  ab_t *p = a->esq;
  a->esq = ab_espelho(a->dir);
  a->dir = ab_espelho(p);
  return a;
}

int *ab_busca_v(ab_t *a, int (*condicao)(int), unsigned int *n_out){
  if (!a) return NULL;
  /*
  EXERCICIO: Completar
  */
  int *v = NULL;
  //int *v = (int *)malloc(...);
  /*
  EXERCICIO: Completar
  */
  return v;
}
