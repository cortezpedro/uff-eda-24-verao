#include <stdio.h>
#include "abb.h"

void imprime_arvbin_de_formas_variadas(ab_t *a){
  printf("preordem: ");
  ab_imprime_preordem(a);
  printf("\n");
  
  printf("simetrica: ");
  ab_imprime_simetrica(a);
  printf("\n");
  
  printf("posordem: ");
  ab_imprime_posordem(a);
  printf("\n");
  
  printf("largura: ");
  ab_imprime_largura(a);
  printf("\n");
}

int main(void){

  ab_t *raiz = ab_cria_no(10);
  raiz->esq = ab_cria_no(5);
  raiz->esq->esq = ab_cria_no(3);
  raiz->esq->dir = ab_cria_no(7);
  raiz->dir = ab_cria_no(11);
  raiz->dir->dir = ab_cria_no(12);
  raiz->dir->dir->dir = ab_cria_no(13);
  
  imprime_arvbin_de_formas_variadas(raiz);
  
  printf("altura da arvore: %u\n",ab_calcula_altura(raiz));
  
  ab_libera(raiz);
  
  printf("\n");
  
  raiz = abb_insere(NULL,10);
  raiz = abb_insere(raiz,5);
  raiz = abb_insere(raiz,3);
  raiz = abb_insere(raiz,7);
  raiz = abb_insere(raiz,11);
  raiz = abb_insere(raiz,12);
  raiz = abb_insere(raiz,13);
  raiz = abb_insere(raiz,9);
  raiz = abb_insere(raiz,15);
  
  imprime_arvbin_de_formas_variadas(raiz);
  
  printf("altura da arvore: %u\n",ab_calcula_altura(raiz));
  
  printf("\n");
  
  raiz = abb_remove(raiz,13);
  
  imprime_arvbin_de_formas_variadas(raiz);
  
  printf("altura da arvore: %u\n",ab_calcula_altura(raiz));
  
  printf("\n");
  
  raiz = abb_remove(raiz,10);
  
  imprime_arvbin_de_formas_variadas(raiz);
  
  printf("altura da arvore: %u\n",ab_calcula_altura(raiz));
  
  int minha_condicao(int a){ return a&1 && (a%5)<3; }//{ return a>10; }//{ return !(a%3); }
  unsigned int dim_v=0;;
  int *v = ab_busca_v(raiz,minha_condicao,&dim_v);
  
  for (unsigned int ii=0; ii<dim_v; ii++) printf("%d ",v[ii]);
  printf("\n");
  
  free(v);

  ab_libera(raiz);
  
  return 0;
}
