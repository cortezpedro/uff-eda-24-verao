#include "grafo.h"

void libera_vizinhos(lse_vertice_t *v){
  lse_vertice_t *p;
  while(v){
    p=v;
    v=v->prox;
    free(p);
  }
}

lse_vertice_t *remove_primeira_ocorrencia_em_vizinhos(lse_vertice_t *v, grf_vertice_t *g){
  lse_vertice_t *p=v, *pa=NULL, *p_aux;
  p=v;
  while(p){
    if (p->vertice == g){
      if (pa) pa->prox=p->prox;
      else v=v->prox;
      p_aux=p;
      p=p->prox;
      free(p_aux);
      break;
    } else {
      pa=p;
      p=p->prox;
    }
  }
  return v;
}

lse_vertice_t *remove_ocorrencias_em_vizinhos(lse_vertice_t *v, grf_vertice_t *g){
  lse_vertice_t *p=v, *pa=NULL, *p_aux;
  p=v;
  while(p){
    if (p->vertice == g){
      if (pa) pa->prox=p->prox;
      else v=v->prox;
      p_aux=p;
      p=p->prox;
      free(p_aux);
    } else {
      pa=p;
      p=p->prox;
    }
  }
  return v;
}

grf_vertice_t *grf_inicia(void){
  return NULL;
}

void grf_libera(grf_vertice_t *g){
  grf_vertice_t *pg;
  while(g){
    libera_vizinhos(g->vizinho);
    g->vizinho=NULL;
    pg = g;
    g = g->prox;
    free(pg);
  }
}

void grf_imprime(grf_vertice_t *g){
  lse_vertice_t *pv;
  while(g){
    printf("Vertice %d\n",g->id);
    pv = g->vizinho;
    if (pv) printf("\t|");
    else printf("\t|--isolado");
    while(pv){
      printf("--%d",pv->vertice->id);
      pv = pv->prox;
    }
    printf("\n");
    g = g->prox;
  }
}

grf_vertice_t *grf_busca_vertice(grf_vertice_t *g, unsigned int id){
  while(g){
    if (g->id == id) break;
    g = g->prox;
  }
  return g;
}

grf_vertice_t *grf_insere_vertice(grf_vertice_t *g, unsigned int id){
  grf_vertice_t *pg=g;
  while(pg){
    if (pg->id == id) return g;
    pg=pg->prox;
  }
  grf_vertice_t *v = (grf_vertice_t *)malloc(sizeof(grf_vertice_t));
  v->id = id;
  v->prox = g;
  v->vizinho=NULL;
  return v;
}

grf_vertice_t *grf_remove_vertice(grf_vertice_t *g, unsigned int id){
  grf_vertice_t *v = grf_busca_vertice(g,id);
  if (!v) return g;
  // remove v das listas de vizinhos
  grf_vertice_t *pg=g, *pga=NULL;
  while(pg){
    if (pg != v) pg->vizinho = remove_ocorrencias_em_vizinhos(pg->vizinho,v);
    pg=pg->prox;
  }
  // libera lista de vizinhos de v
  libera_vizinhos(v->vizinho);
  // remove v da lista de vertices
  pg=g;
  while(pg){
    if (pg == v){
      if (pga) pga->prox = pg->prox;
      else g=g->prox;
      free(v);
      break;
    }
    pga=pg;
    pg=pg->prox;
  }
  return g;
}

void grf_insere_aresta(grf_vertice_t *g, unsigned int id1, unsigned int id2, int ambas_direcoes){
  grf_vertice_t *v1 = grf_busca_vertice(g,id1);
  grf_vertice_t *v2 = grf_busca_vertice(g,id2);
  if (!v1 || !v2) return;
  lse_vertice_t *p = (lse_vertice_t *)malloc(sizeof(lse_vertice_t));
  p->vertice = v2;
  p->prox = v1->vizinho;
  v1->vizinho = p;
  if (!ambas_direcoes) return;
  p = (lse_vertice_t *)malloc(sizeof(lse_vertice_t));
  p->vertice = v1;
  p->prox = v2->vizinho;
  v2->vizinho = p;
}

void grf_remove_aresta(grf_vertice_t *g, unsigned int id1, unsigned int id2, int ambas_direcoes){
  grf_vertice_t *v1 = grf_busca_vertice(g,id1);
  grf_vertice_t *v2 = grf_busca_vertice(g,id2);
  if (!v1 || !v2) return;
  v1->vizinho = remove_primeira_ocorrencia_em_vizinhos(v1->vizinho,v2);
  if (!ambas_direcoes) return;
  v2->vizinho = remove_primeira_ocorrencia_em_vizinhos(v2->vizinho,v1);
}

int grf_tem_caminho_bfs(grf_vertice_t *g, unsigned int id1, unsigned int id2){
  grf_vertice_t *v1 = grf_busca_vertice(g,id1);
  grf_vertice_t *v2 = grf_busca_vertice(g,id2);
  if (!v1 || !v2) return 0;
  int achou_caminho=0;
  /*
  EXERCICIO: Completar
  */
  return achou_caminho;
}

unsigned int **grf_monta_matriz_adjacencia(grf_vertice_t *g){
  unsigned int **m = NULL;
  /*
  EXERCICIO: Completar
  */
  return m;
}
