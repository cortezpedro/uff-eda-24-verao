#include "lse.h"

lse_t *lse_inicia(void){
    return NULL;
}

void lse_libera(lse_t *l){
    lse_t *p;
    while(l){
        p=l;
        l=l->prox;
        free(p);
    }
}

void lse_imprime(lse_t *l){
    while(l) { printf("%d ",l->dado); l=l->prox; }
    printf("\n");
}

lse_t *lse_insere(lse_t *l, int d){
    lse_t *no = (lse_t *)malloc(sizeof(lse_t));
    no->dado=d;
    no->prox=NULL;
    if (!l) return no;
    lse_t *p = l->prox;
    l->prox = no;
    no->prox = p;
    return l;
}

lse_t *lse_insere_no_inicio(lse_t *l, int d){
    lse_t *p=lse_insere(NULL,d);
    p->prox=l;
    return p;
}

lse_t *lse_insere_no_fim(lse_t *l, int d){
    if (!l) return lse_insere(NULL,d);
    lse_t *p=l;
    while(p->prox) p=p->prox;
    p=lse_insere(p,d);
    return l;
}

lse_t *lse_insere_ordenado(lse_t *l, int d){
    if (!l) return lse_insere(NULL,d);
    if (d < l->dado) return lse_insere_no_inicio(l,d);
    lse_t *p=l;
    while(p->prox){
        if (d < p->prox->dado) break;
        p=p->prox;
    }
    p=lse_insere(p,d);
    return l;
}

lse_t *lse_remove(lse_t *l, lse_t **anteprox){
    if (!l) return NULL;
    lse_t *p = l->prox;
    free(l);
    if (anteprox) *anteprox=p;
    return p;
}

lse_t *lse_remove_no_inicio(lse_t *l){
    return lse_remove(l,NULL);
}

lse_t *lse_remove_no_fim(lse_t *l){
    if (!l) return NULL;
    lse_t *a=l, *p=l->prox;
    if (!p) return lse_remove_no_inicio(a);
    while (p->prox){
        a=a->prox;
        p=p->prox;
    }
    p=lse_remove(p,&(a->prox));
    return l;
}

lse_t *lse_busca(lse_t *l, int d){
    while (l){
        if (l->dado == d) break;
        l=l->prox;
    }
    return l;
}

lse_t *lse_busca_min(lse_t *l){
    if (!l) return NULL;
    int val=l->dado;
    lse_t *p=l;
    l=l->prox;
    while (l){
        if (l->dado < val){
            val=l->dado;
            p=l;
        }
        l=l->prox;
    }
    return p;
}

lse_t *lse_busca_max(lse_t *l){
    if (!l) return NULL;
    int val=l->dado;
    lse_t *p=l;
    l=l->prox;
    while (l){
        if (l->dado > val){
            val=l->dado;
            p=l;
        }
        l=l->prox;
    }
    return p;
}

lse_t *lse_ordena(lse_t *l){
    if (!l) return NULL;
    lse_t *lo=lse_inicia();
    while(l){
        lo=lse_insere_ordenado(lo,l->dado);
        l=lse_remove_no_inicio(l);
    }
    return lo;
}

lse_t *lse_merge_ordenado(lse_t *l1, lse_t *l2){
    if (!l1) return l2;
    if (!l2) return l1;
    lse_t *lo, *po;
    if (l1->dado < l2->dado){
      lo=l1;
      l1=l1->prox;
    } else {
      lo=l2;
      l2=l2->prox;
    }
    po=lo;
    po->prox=NULL;
    while(l1 && l2){
      if (l1->dado < l2->dado){
        po->prox=l1;
        l1=l1->prox;
      } else {
        po->prox=l2;
        l2=l2->prox;
      }
      po = po->prox;
      po->prox=NULL;
    }
    po->prox = l1 ? l1 : l2;
    return lo;
}

lse_t *mergesort(lse_t *l, const unsigned int dim){
  if (dim<2) return l;
  lse_t *p=l, *lhalf;
  const unsigned int nhalf = (dim/2);
  for (unsigned int i=0; i<(nhalf-1); i++) p=p->prox;
  lhalf = p->prox;
  p->prox=NULL;
  l=mergesort(l,nhalf);
  lhalf=mergesort(lhalf,(dim-1)/2+1);
  return lse_merge_ordenado(l,lhalf);
}

lse_t * lse_ordena_mergesort(lse_t *l){
  lse_t *p=l;
  unsigned int n=0;
  while(p){
    n++;
    p=p->prox;
  }
  return mergesort(l,n);
}
