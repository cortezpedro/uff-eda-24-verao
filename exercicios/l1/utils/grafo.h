#ifndef MY_GRAPH_INCLUDE
#define MY_GRAPH_INCLUDE

#include <stdio.h>
#include <stdlib.h>

typedef struct _lse_vertice lse_vertice_t; // tipo para lista de vertices vizinhos a determinado vertice
typedef struct _grf_vertice grf_vertice_t; // tipo para lista de vertices do grafo

struct _lse_vertice{
  grf_vertice_t *vertice;
  lse_vertice_t *prox;
};

struct _grf_vertice{
    unsigned int id;
    grf_vertice_t *prox;
    lse_vertice_t *vizinho;
};

grf_vertice_t *grf_inicia(void);
void grf_libera( grf_vertice_t *g);
void grf_imprime(grf_vertice_t *g);

grf_vertice_t *grf_busca_vertice( grf_vertice_t *g, unsigned int id);
grf_vertice_t *grf_insere_vertice(grf_vertice_t *g, unsigned int id);
grf_vertice_t *grf_remove_vertice(grf_vertice_t *g, unsigned int id);

void grf_insere_aresta(grf_vertice_t *g, unsigned int id1, unsigned int id2, int ambas_direcoes);
void grf_remove_aresta(grf_vertice_t *g, unsigned int id1, unsigned int id2, int ambas_direcoes);

//int grf_tem_caminho_bfs(grf_vertice_t *g, unsigned int id1, unsigned int id2);

//unsigned int **grf_monta_matriz_adjacencia(grf_vertice_t *g);

#endif // MY_GRAPH_INCLUDE
