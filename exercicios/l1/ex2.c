#include <stdio.h>
#include <stdlib.h>

#include "utils/lse.h"
#include "utils/ab.h"
#include "utils/abb.h"
#include "utils/avl.h"
#include "utils/grafo.h"

unsigned int conta_ocorrencias(abb_t *a, int (*f)(int)){
  if (!a) return 0;
  unsigned int ne, nd;
  ne = conta_ocorrencias(a->esq,f);
  nd = conta_ocorrencias(a->dir,f);
  return ne+nd + ( f(a->dado) != 0 );
}

int *insere_ocorrencias(int *v, abb_t *a, int (*f)(int)){
  if (!a) return v;
  int *p = insere_ocorrencias(v,a->esq,f);
  if (f(a->dado)) *(p++) = a->dado;
  p = insere_ocorrencias(p,a->dir,f);
  return p;
}

int *abb_busca_condicao(abb_t *a, int (*condicao)(int), unsigned int *n_out){
  if (!a) return NULL;
  unsigned int ne, nd, n;
  int c = condicao(a->dado);
  ne = conta_ocorrencias(a->esq,condicao);
  nd = conta_ocorrencias(a->dir,condicao);
  n = ne+nd + ( c != 0 );
  if (n_out) *n_out = n;
  if (!n) return NULL;
  int *v = (int *)malloc(sizeof(int)*n);
  int *p = insere_ocorrencias(v,a->esq,condicao);
  if (c) *(p++) = a->dado;
  p = insere_ocorrencias(p,a->dir,condicao);
  return v;
}

void imprime_arvbin_de_formas_variadas(ab_t *a){
  printf("preordem: ");
  ab_imprime_preordem(a);
  printf("\n");
  
  printf("simetrica: ");
  ab_imprime_simetrica(a);
  printf("\n");
  
  printf("posordem: ");
  ab_imprime_posordem(a);
  printf("\n");
  
  printf("largura: ");
  ab_imprime_largura(a);
  printf("\n");
}

int main(void){
  
  abb_t *raiz = abb_insere(NULL,10);
  raiz = abb_insere(raiz,5);
  raiz = abb_insere(raiz,3);
  raiz = abb_insere(raiz,7);
  raiz = abb_insere(raiz,11);
  raiz = abb_insere(raiz,12);
  raiz = abb_insere(raiz,13);
  raiz = abb_insere(raiz,9);
  raiz = abb_insere(raiz,15);
  
  raiz = abb_remove(raiz,13);
  raiz = abb_remove(raiz,10);
  
  imprime_arvbin_de_formas_variadas(raiz);
  
  printf("altura da arvore: %u\n",ab_calcula_altura(raiz));
  
  int minha_condicao(int a){ return !(a%3); }
  unsigned int dim_v=0;;
  int *v = abb_busca_condicao(raiz,minha_condicao,&dim_v);
  
  for (unsigned int ii=0; ii<dim_v; ii++) printf("%d ",v[ii]);
  printf("\n");
  
  free(v);
  
  ab_libera(raiz);
  
  return 0;
}
