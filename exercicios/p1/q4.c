#include <stdio.h>
#include <stdlib.h>

#include "utils/abb.h"

int checa_se_impar_entre_xy(int d, int x, int y){
  return (d > x && d < y && d&1 );
}

unsigned int conta_impares_entre_xy(abb_t *a, int x, int y){
  if (!a) return 0;
  unsigned int ne=0, nd=0;
  if (a->dado > x) ne=conta_impares_entre_xy(a->esq,x,y);
  if (a->dado < y) nd=conta_impares_entre_xy(a->dir,x,y);
  return ne+nd + checa_se_impar_entre_xy(a->dado,x,y);
}

int *insere_impares_entre_xy(abb_t *a, int x, int y, int *v){
  if (!a) return v;
  int *p=v;
  if (a->dado < y) p=insere_impares_entre_xy(a->dir,x,y,p);
  if (checa_se_impar_entre_xy(a->dado,x,y)) *(p++)=a->dado;
  if (a->dado > x) p=insere_impares_entre_xy(a->esq,x,y,p);
  return p;
}

int *abb_busca_impares_entre_xy(abb_t *a, int x, int y, int *n_saida){
  if (!a || x>=y) return NULL;
  unsigned int n;
  n = conta_impares_entre_xy(a,x,y);
  if (n_saida) *n_saida = n;
  if (!n) return NULL;
  int *v = (int *)malloc(sizeof(int)*n);
  insere_impares_entre_xy(a,x,y,v);
  return v;
}

void imprime_arvbin_de_formas_variadas(ab_t *a){
  printf("preordem: ");
  ab_imprime_preordem(a);
  printf("\n");
  
  printf("simetrica: ");
  ab_imprime_simetrica(a);
  printf("\n");
  
  printf("posordem: ");
  ab_imprime_posordem(a);
  printf("\n");
  
  printf("largura: ");
  ab_imprime_largura(a);
  printf("\n");
}

int main(void){
  
  abb_t *raiz = abb_insere(NULL,35);
  raiz = abb_insere(raiz,10);
  raiz = abb_insere(raiz,50);
  raiz = abb_insere(raiz,9);
  raiz = abb_insere(raiz,12);
  raiz = abb_insere(raiz,38);
  raiz = abb_insere(raiz,52);
  raiz = abb_insere(raiz,8);
  raiz = abb_insere(raiz,11);
  raiz = abb_insere(raiz,19);
  raiz = abb_insere(raiz,57);
  
  imprime_arvbin_de_formas_variadas(raiz);
  
  int dim_v=0;
  int *v = abb_busca_impares_entre_xy(raiz,7,51,&dim_v);
  for (unsigned int ii=0; ii<dim_v; ii++) printf("%d ",v[ii]);
  if (!v) printf("Vetor vazio.");
  printf("\n");
  
  if (v) free(v);
  
  ab_libera(raiz);
  
  return 0;
}
