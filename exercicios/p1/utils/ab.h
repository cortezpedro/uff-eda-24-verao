#ifndef MY_AB_H_INCLUDE
#define MY_AB_H_INCLUDE

#include <stdio.h>
#include <stdlib.h>

typedef struct _ab{
    int dado;
    struct _ab *esq;
    struct _ab *dir;
} ab_t;

ab_t *ab_cria_no(int d);
void ab_libera(ab_t *a);

unsigned int ab_calcula_altura(ab_t *a);

void ab_imprime_preordem(ab_t *a);
void ab_imprime_simetrica(ab_t *a);
void ab_imprime_posordem(ab_t *a);
void ab_imprime_largura(ab_t *a);

void ab_imprime_preordem_pilha(ab_t *a);
void ab_imprime_simetrica_pilha(ab_t *a);
void ab_imprime_posordem_pilha(ab_t *a);

unsigned int ab_cheia(ab_t *a);
ab_t * ab_espelho(ab_t *a);

int *ab_busca_v(ab_t *a, int (*condicao)(int), unsigned int *n_out);

#endif // MY_AB_H_INCLUDE
