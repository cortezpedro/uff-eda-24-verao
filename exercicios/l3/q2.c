#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/aluno.h"

void ordena_arq_bin_bubblesort(FILE *arq){
  if (!arq) return;
  size_t n = tamanho_arquivo_bin(arq);
  aluno_t a, b;
  int houve_troca=1;
  for (size_t i=0; i<n && houve_troca; i++){
    houve_troca=0;
    for (size_t j=0; j<(n-1); j++){
      fseek(arq,j*sizeof(aluno_t),SEEK_SET);
      le_aluno_bin(arq,&a);
      le_aluno_bin(arq,&b);
      if (b.nota > a.nota){
        fseek(arq,j*sizeof(aluno_t),SEEK_SET);
        salva_aluno_bin(arq,&b);
        salva_aluno_bin(arq,&a);
        fflush(arq);
        houve_troca=1;
      }
    }
  }
}

int main(void){

  char filename[] = "turma.bin";
  
  FILE *f = fopen(filename,"rb+");
  if (!f) {printf("Nao consegui abrir \"%s\"\n",filename); return 1;}
  imprime_conteudo_arquivo_bin(f); rewind(f); printf("\n##################\n\n");
  ordena_arq_bin_bubblesort(f);
  rewind(f); imprime_conteudo_arquivo_bin(f);
  fclose(f);

  return 0;
}
