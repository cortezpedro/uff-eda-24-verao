#ifndef MY_ARVB_H
#define MY_ARVB_H

#include <stdio.h>
#include <stdlib.h>

typedef struct _no_arvb{
  int m;
  unsigned char folha; // flag para indicar se no folha
  int *chaves;
  struct _no_arvb **filhos;
  struct _no_arvb *pai;
} arvb_t;

arvb_t *arvb_inicia(void);
arvb_t *arvb_cria_pagina(int s, unsigned int d);
void arvb_libera(arvb_t *a);
void arvb_imprime(arvb_t *a, int tab);

arvb_t *arvb_busca(arvb_t *a, int s);
arvb_t *arvb_insere(arvb_t *a, int s, unsigned int d);
arvb_t *arvb_remove(arvb_t *a, int s, unsigned int d);

#endif
