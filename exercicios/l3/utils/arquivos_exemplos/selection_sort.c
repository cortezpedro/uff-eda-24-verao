#include <stdio.h>
#include <stdlib.h>

typedef struct _aluno{
  int mat;
  char nome[128];
  unsigned int nota;
} aluno_t;


void le_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fread(a,sizeof(aluno_t),1,f);
}

void salva_aluno(FILE *f, aluno_t *a){
  if (!f || !a) return;
  fwrite(a,sizeof(aluno_t),1,f);
}

void imprime_aluno(aluno_t *a){
  if (!a) return;
  printf("[%d] %s: %d\n",a->mat,a->nome,a->nota);
}

size_t tamanho_arquivo(FILE *f){
  fseek(f,0,SEEK_END);
  size_t tam = ftell(f) / sizeof(aluno_t);
  rewind(f);
  return tam;
}

void imprime_conteudo_arquivo(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo(f);
  if (!n) return;
  aluno_t *dados = (aluno_t *)malloc(sizeof(aluno_t)*n);
  for (int i=0; i<n; i++) le_aluno(f,&dados[i]);
  for (int i=0; i<n; i++) imprime_aluno(&dados[i]);
  free(dados);
}

void ordena_por_nota_em_memoria_selection_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo(f); // faz um rewind em f
  if (!n) return;
  aluno_t *turma = (aluno_t *)malloc(sizeof(aluno_t)*n);
  fread(turma,sizeof(aluno_t),n,f);
  aluno_t a;
  int i_min;
  for (int i=0; i<(n-1); i++){
    a = turma[i]; i_min=i;
    for (int j=i+1; j<n; j++){
      if (turma[j].nota > a.nota){
        a = turma[j]; i_min=j;
      }
    }
    turma[i_min]=turma[i];
    turma[i]=a;
  }
  rewind(f);
  fwrite(turma,sizeof(aluno_t),n,f);
  fflush(f);
  free(turma);
}

void ordena_por_nota_em_disco_selection_sort(FILE *f){
  if (!f) return;
  size_t n = tamanho_arquivo(f); // faz um rewind em f
  if (!n) return;
  aluno_t a, b;
  int i_min;
  for (int i=0; i<n; i++){
    fseek(f,i*sizeof(aluno_t),SEEK_SET);
    le_aluno(f,&a); i_min=i;
    for (int j=i+1; j<n; j++){
      le_aluno(f,&b);
      if (b.nota < a.nota){
        a = b; i_min=j;
      }
    }
    if (i_min != i) {
      fseek(f,i*sizeof(aluno_t),SEEK_SET);
      le_aluno(f,&b);
      fseek(f,(i_min)*sizeof(aluno_t),SEEK_SET); 
      salva_aluno(f,&b);
    }
    fseek(f,i*sizeof(aluno_t),SEEK_SET);
    salva_aluno(f,&a);
    fflush(f);
  }
}

int main(void){

  aluno_t turma[]={
    {0,"Jonas",8},
    {1,"Marta",5},
    {2,"Francisco",3},
    {3,"Renato",9},
    {4,"Luan",6},
    {5,"Marcia",4},
    {6,"Maria",9}
  };
  const int dim = sizeof(turma) / sizeof(aluno_t);
  
  //////////////////////////////////////////////////////
  FILE *f = fopen("turma.bin","wb");
  if (!f) return 1;
  for (int i=0; i<dim; i++) salva_aluno(f,&turma[i]);
  fclose(f);
  
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  
  //////////////////////////////////////////////////////
  printf("\nOrdenando por nota:\n");
  
  f = fopen("turma.bin","rb+");
  if (!f) return 1;
  //ordena_por_nota_em_memoria_insertion_sort(f);
  //ordena_por_nota_em_disco_insertion_sort(f);
  //ordena_por_nota_em_memoria_selection_sort(f);
  ordena_por_nota_em_disco_selection_sort(f);
  fclose(f);
  
  f = fopen("turma.bin","rb");
  if (!f) return 1;
  imprime_conteudo_arquivo(f);
  fclose(f);
  //////////////////////////////////////////////////////
  
  return 0;
}
