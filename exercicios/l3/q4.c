#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/arvB.h"

int arvb_max(arvb_t *a){
  if (!a) return -1; // seguranca
  if (a->folha) return a->chaves[a->m-1];
  return arvb_max(a->filhos[a->m]);
}

int arvb_min(arvb_t *a){
  if (!a) return -1; // seguranca
  if (a->folha) return a->chaves[0];
  return arvb_min(a->filhos[0]);
}

int arvb_maiorque(arvb_t *a, int x){
  arvb_t *no = arvb_busca(a,x); // no onde x esta ou deveria estar
  if (!no) return -1; // seguranca
  unsigned int j;
  if (!no->folha){
    for (j=0; j<no->m && no->chaves[j]<=x; j++) ;
    no=no->filhos[j];
    while (!no->folha) no=no->filhos[0];
    return no->chaves[0];
  }
  do {
    for (j=0; j<no->m && no->chaves[j]<=x; j++) ;
    if (j<no->m) return no->chaves[j];
    no=no->pai;
  } while (no);
  // se chegou aqui, nao achou (x>=max(a))
  return -1;
}

int main(void){

  const int ordem = 2;

  arvb_t *raiz = arvb_inicia();
  
  int arr[] = {1,2,15,20,9,3,4,15,20,30,40,46,50,51,52,60,65,70,56,58,80,85,90,57,71,72,73};
  const int arr_dim = sizeof(arr)/sizeof(int);
  for (int i=0; i<arr_dim; i++) raiz = arvb_insere(raiz,arr[i],ordem);
  arvb_imprime(raiz,0);
  printf("\n");
  
  // CHAMADA DAS FUNCOES
  printf("chave max: %d\n",arvb_max(raiz));
  printf("chave min: %d\n",arvb_min(raiz));
  int x=46;
  printf("chave imediatamente maior que %d: %d\n",x,arvb_maiorque(raiz,x));
  
  arvb_libera(raiz);

  return 0;
}
