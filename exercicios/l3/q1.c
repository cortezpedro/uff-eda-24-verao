#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/aluno.h"

typedef struct _aux_fila{
  unsigned int n;
  FILE *f;
  struct _aux_fila *prox;
} fila_part_t;

fila_part_t *push_fim(fila_part_t *fila, unsigned int n, FILE *f){
  fila_part_t *novo = (fila_part_t *)malloc(sizeof(fila_part_t));
  novo->n=n;
  novo->f=f;
  novo->prox=NULL;
  if (fila) fila->prox=novo;
  return novo;
}

fila_part_t *pop_inicio(fila_part_t *fila){
  if (!fila) return NULL;
  fila_part_t *p = fila->prox;
  free(fila);
  return p;
}

size_t estima_custo_io_substituicao(size_t n, unsigned int m, unsigned int f){
  unsigned int part = (2*m);
  size_t np = 1 + (n-1) / part;
  size_t custo_io=n;
  if (np < f) return custo_io+n;
  fila_part_t *inicio=NULL, *fim=NULL;
  inicio=push_fim(NULL,part,NULL); fim=inicio;
  for (size_t i=1; i<np; i++) fim=push_fim(fim,part,NULL);
  for (long i=np; i>1; i-=(f-2)){
    fim=push_fim(fim,inicio->n,NULL);
    inicio=pop_inicio(inicio);
    for (unsigned int j=1; j<(f-1) && inicio != fim; j++){
      fim->n += inicio->n;
      inicio=pop_inicio(inicio);
    }
    custo_io += fim->n;
  } while(inicio) inicio=pop_inicio(inicio);
  return custo_io;
}

size_t estima_custo_io_natural(size_t n, unsigned int m, unsigned int f){
  unsigned int part = (unsigned int)(2.718*(float)m);
  size_t np = 1 + (n-1) / part;
  size_t custo_io=n+(np-1)*m;
  if (np < f) return custo_io+n;
  fila_part_t *inicio=NULL, *fim=NULL;
  inicio=push_fim(NULL,part,NULL); fim=inicio;
  for (size_t i=1; i<np; i++) fim=push_fim(fim,part,NULL);
  for (long i=np; i>1; i-=(f-2)){
    fim=push_fim(fim,inicio->n,NULL);
    inicio=pop_inicio(inicio);
    for (unsigned int j=1; j<(f-1) && inicio != fim; j++){
      fim->n += inicio->n;
      inicio=pop_inicio(inicio);
    }
    custo_io += fim->n;
  } while(inicio) inicio=pop_inicio(inicio);
  return custo_io;
}

void ordena_arq_txt(FILE *arq, unsigned int m, unsigned int f){
  size_t n = linhas_arquivo_txt(arq);
  size_t np;
  
  // Escolhe algoritmo de geracao que deve causar menos io
  if ( estima_custo_io_substituicao(n,m,f) > estima_custo_io_natural(n,m,f) )
   np = gera_particoes_selecao_natural(arq,n,m);
  else np = gera_particoes_selecao_substituicao(arq,n,m);
  
  // vetor com (f-1) FILE *, para input da intercalacao
  FILE **parts = (FILE **)malloc(sizeof(FILE *)*(f-1));
  
  // abre todas as particoes para leitura e insere em fila
  char filename[256];
  fila_part_t *inicio=NULL, *fim=NULL;
  for (size_t i=0; i<np; i++){
    sprintf(filename,"particao_%zu.txt",i);
    if (inicio) fim=push_fim(fim,0,fopen(filename,"r"));
    else {
      inicio=push_fim(NULL,0,fopen(filename,"r"));
      fim=inicio;
    }
    if (!fim->f){
      while(inicio){
        fclose(inicio->f);
        inicio=pop_inicio(inicio);
      }
      free(parts);
      printf("Tentou abrir \"%s\" e falhou.\n",filename);
      return;
    }
  }
  
  // volta cursor do arq saida pro inicio apos ter lido inteiro na geracao de particoes
  rewind(arq);
  
  // checa se da pra intercalar tudo de uma vez
  if (np < f){
    FILE **p_parts = parts;
    while (inicio){
      *(p_parts++) = inicio->f;
      inicio=pop_inicio(inicio);
    }
    intercala_particoes_arvbin_vencedores(arq,parts,np);
    for (unsigned int k=0; k<np; k++) fclose(parts[k]);
    free(parts);
    return;
  }
  
  // algoritmo da fila (intercalacao otima)
  unsigned int j;
  while(inicio->prox){
    fim=push_fim(fim,0,NULL);
    for (j=0; j<(f-1) && inicio != fim; j++){
      parts[j] = inicio->f;
      inicio=pop_inicio(inicio);
    }
    if (inicio == fim) fim->f = arq;
    else {
      sprintf(filename,"particao_%zu.txt",np++);
      fim->f = fopen(filename,"w+");
      if (!fim->f){
        while(inicio) {
          fclose(inicio->f);
          inicio=pop_inicio(inicio);
        }
        free(parts);
        return;
      }
    }
    intercala_particoes_arvbin_vencedores(fim->f,parts, j>=(f-1) ? (f-1) : j );
    for (unsigned int k=0; k<j; k++) fclose(parts[k]);
    if (fim->f != arq) rewind(fim->f);
  } while(inicio) inicio=pop_inicio(inicio);
  
  free(parts);
}

int main(void){

//  printf("Cenario 1 (sub): %zu\n",estima_custo_io_substituicao(400000,20000,10));
//  printf("Cenario 1 (nat): %zu\n",estima_custo_io_natural(400000,20000,10));
//  printf("Cenario 2 (sub): %zu\n",estima_custo_io_substituicao(400000,10000,10));
//  printf("Cenario 2 (nat): %zu\n",estima_custo_io_natural(400000,10000,10));

  char filename[] = "turma.txt";
  
  const unsigned int m = 4; // max de 4 registros em memoria simultaneamente
  const unsigned int f = 3; // max de 3 arquivos abertos simultaneamente
  
  FILE *arq = fopen(filename,"r+");
  if (!arq) {printf("Nao consegui abrir \"%s\"\n",filename); return 1;}
  ordena_arq_txt(arq,m,f);
  fclose(arq);

  return 0;
}
