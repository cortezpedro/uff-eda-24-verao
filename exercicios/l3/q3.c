#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "utils/arvB.h"

int impar_entre_xy(int dado, int x, int y){
  return ( dado&1 && (dado > x && dado < y) );
}

int conta_impares_entre_xy(arvb_t *a, int x, int y){
  if (!a) return 0;
  int n=0;
  unsigned int j;
  for (j=0; j<a->m && a->chaves[j]<=x; j++) ;
  n+=conta_impares_entre_xy(a->filhos[j],x,y);
  for ( ; j<a->m && a->chaves[j]<y; j++){
    n+=impar_entre_xy(a->chaves[j],x,y);
    n+=conta_impares_entre_xy(a->filhos[j+1],x,y);
  }
  return n;
}

int *insere_impares_entre_xy(arvb_t *a, int x, int y, int *p){
  if (!a) return p;
  unsigned int j;
  for (j=0; j<a->m && a->chaves[j]<=x; j++) ;
  p=insere_impares_entre_xy(a->filhos[j],x,y,p);
  for ( ; j<a->m && a->chaves[j]<y; j++){
    if (impar_entre_xy(a->chaves[j],x,y)) *(p++)=a->chaves[j];
    p=insere_impares_entre_xy(a->filhos[j+1],x,y,p);
  }
  return p;
}

int *insere_impares_entre_xy_decrescente(arvb_t *a, int x, int y, int *p){
  if (!a) return p;
  int j;
  for (j=a->m-1; j>=0 && a->chaves[j]>=y; j--) ;
  p=insere_impares_entre_xy_decrescente(a->filhos[j+1],x,y,p);
  for ( ; j>=0 && a->chaves[j]>x; j--){
    if (impar_entre_xy(a->chaves[j],x,y)) *(p++)=a->chaves[j];
    p=insere_impares_entre_xy_decrescente(a->filhos[j],x,y,p);
  }
  return p;
}

int *arvb_impares_entre_xy(arvb_t *a, int x, int y, int *n_saida){
  if (!a) return NULL;
  int n = conta_impares_entre_xy(a,x,y);
  *n_saida=n;
  if (!n) return NULL;
  int *v = (int *)malloc(sizeof(int)*n);
  insere_impares_entre_xy(a,x,y,v);
  //insere_impares_entre_xy_decrescente(a,x,y,v);
  return v;
}

int main(void){

  /*
  const int ordem = 3;

  arvb_t *raiz = arvb_inicia();
  
  raiz = arvb_insere(raiz,8,ordem);
  raiz = arvb_insere(raiz,1,ordem);
  raiz = arvb_insere(raiz,6,ordem);
  raiz = arvb_insere(raiz,3,ordem);
  raiz = arvb_insere(raiz,14,ordem);
  raiz = arvb_insere(raiz,36,ordem);
  raiz = arvb_insere(raiz,32,ordem);
  raiz = arvb_insere(raiz,43,ordem);
  raiz = arvb_insere(raiz,39,ordem);
  raiz = arvb_insere(raiz,41,ordem);
  raiz = arvb_insere(raiz,38,ordem);
  raiz = arvb_insere(raiz,4,ordem);
  raiz = arvb_insere(raiz,5,ordem);
  raiz = arvb_insere(raiz,42,ordem);
  raiz = arvb_insere(raiz,2,ordem);
  raiz = arvb_insere(raiz,7,ordem);
  
  arvb_imprime(raiz,0);
  */
  
  const int ordem = 2;

  arvb_t *raiz = arvb_inicia();
  
  int arr[] = {1,2,15,20,9,3,4,15,20,30,40,46,50,51,52,60,65,70,56,58,80,85,90};
  const int arr_dim = sizeof(arr)/sizeof(int);
  for (int i=0; i<arr_dim; i++) raiz = arvb_insere(raiz,arr[i],ordem);
  arvb_imprime(raiz,0);
  
  // CHAMADA DA FUNCAO
  int n_saida=0;
  int x=2, y=52;
  int *v = arvb_impares_entre_xy(raiz,x,y,&n_saida);
  printf("\n# de impares entre %d e %d: %d\n",x,y,n_saida);
  if (v){
    printf("\nimpares entre %d e %d:",x,y);
    for (int i=0; i<n_saida; i++) printf(" %d",v[i]);
    printf("\n\n");
    free(v);
  }
  
  arvb_libera(raiz);

  return 0;
}
